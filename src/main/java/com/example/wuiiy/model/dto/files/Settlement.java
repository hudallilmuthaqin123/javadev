package com.example.wuiiy.model.dto.files;

import java.util.List;

public class Settlement {
    public String header;

    public List<String> data;

    public String footer;

    public Settlement() {
    }

    public Settlement(String header, List<String> data, String footer) {
        this.header = header;
        this.data = data;
        this.footer = footer;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
}
