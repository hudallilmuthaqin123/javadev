package com.example.wuiiy.helpers.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.util.Map;
import java.util.Optional;

public class jwt {
    private static final SecretKey SECRET_KEY = Keys.hmacShaKeyFor("YourSecretKeyYourSecretKeyYourSecretKeyYourSecretKey".getBytes());

    public static String encode(Map<String, Object> data) {
        return Jwts.builder()
                .setClaims(data)
                .signWith(SECRET_KEY, SignatureAlgorithm.HS256)
                .compact();
    }

    public static Optional<Map<String, Object>> decode(String encodedJwt) {
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(SECRET_KEY)
                    .build()
                    .parseClaimsJws(encodedJwt)
                    .getBody();
            return Optional.of(claims);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
