package com.example.wuiiy.helpers.json;

import com.example.wuiiy.helpers.exceptions.DefinedException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

public class JsonUtils {

    public static <T> T json_data(String requestBody, String key, Class<T> type, boolean isRequired, Wrapper<T> wrapper) throws DefinedException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Map<String, Object> data = objectMapper.readValue(requestBody, Map.class);
            if (!data.containsKey(key)) {
                if (isRequired) {
                    throw new DefinedException("3", "Missing required parameter: " + key);
                } else {
                    return null;
                }
            }

            Object value = data.get(key);
            if (isRequired && value == null) {
                throw new DefinedException("4", "Value for parameter '" + key + "' is null");
            }

            // Check type
            if (isRequired && !type.isInstance(value)) {
                throw new DefinedException("4", "Invalid type for parameter '" + key + "'. Expected: " + type.getName());
            }

            if (wrapper != null) {
                return wrapper.wrap((T) value);
            } else {
                return (T) value;
            }
        } catch (IOException e) {
            throw new DefinedException("2", "Error decoding JSON data: " + e.getMessage());
        }
    }

    public interface Wrapper<T> {
        T wrap(T value);
    }
}
