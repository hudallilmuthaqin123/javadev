package com.example.wuiiy.helpers.validator;

import com.example.wuiiy.helpers.exceptions.DefinedException;
import com.example.wuiiy.helpers.exceptions.ErrorMessages;
import com.example.wuiiy.helpers.exceptions.ExceptionDetail;

import java.util.HashMap;
import java.util.Map;

public class SecurePassword {

    public static void validatePassword(String password) {
        if (password.length() < 8) {
            Map<String, String> params = new HashMap<>();
            params.put("param", password);
            params.put("something", String.join(", ", new String[]{"at least 8 characters"}));
            String errorMsg = ErrorMessages.getErrorMessage("4", params);
            throw new DefinedException("4", new ExceptionDetail.PasswordError(password), errorMsg);
        }
    }
}
