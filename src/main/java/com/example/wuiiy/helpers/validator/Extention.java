package com.example.wuiiy.helpers.validator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.wuiiy.helpers.exceptions.DefinedException;
import com.example.wuiiy.helpers.exceptions.ErrorMessages;
import com.example.wuiiy.helpers.exceptions.ExceptionDetail;

public class Extention {
    public static final String[] IMAGES = {".jpeg",".jpg",".jpeg",".png"};
    public static final String[] IDENTITY_DOCS = {".txt",".pdf"};
    public static void validateDocs(String fileName) {
        List<String> allowedExts = Arrays.asList(Extention.IDENTITY_DOCS);
        String ext = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        if (!allowedExts.contains(ext)) {
            Map<String, String> params = new HashMap<>();
            params.put("ext", ext);
            params.put("alloweds_ext", String.join(", ", allowedExts));
            String errorMsg = ErrorMessages.getErrorMessage("9", params);
            throw new DefinedException("9", new ExceptionDetail.ExtentionNotAllowed(ext, allowedExts), errorMsg);
        }
    }

    public static void validateImgs(String fileName) {
        List<String> allowedExts = Arrays.asList(Extention.IMAGES);
        String ext = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        if (!allowedExts.contains(ext)) {
            Map<String, String> params = new HashMap<>();
            params.put("ext", ext);
            params.put("alloweds_ext", String.join(", ", allowedExts));
            String errorMsg = ErrorMessages.getErrorMessage("9", params);
            throw new DefinedException("9", new ExceptionDetail.ExtentionNotAllowed(ext, allowedExts), errorMsg);
        }
    }

}
