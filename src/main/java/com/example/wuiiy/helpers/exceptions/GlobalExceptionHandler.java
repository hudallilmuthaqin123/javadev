package com.example.wuiiy.helpers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(DefinedException.class)
    public ResponseEntity<Object> handleDefinedException(DefinedException ex, HttpServletRequest request) {
        String errorCode = ex.getCode();
        String detail = ex.getDetail().toString();
        String path = request.getRequestURI();

        Map<String, Object> response = new HashMap<>();
        response.put("error_code", errorCode);
        response.put("detail", detail);
        response.put("message", "Error occurred in service: " + ex.getMessage());
        response.put("path", path);

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}