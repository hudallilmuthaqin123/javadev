package com.example.wuiiy.helpers.exceptions;

import java.util.List;

public class ExceptionDetail {
    public static class UsernameNotFound{
        public String username;
        public UsernameNotFound(String username) {
            this.username = username;
        }
        @Override
        public String toString() {
            return "{" + "username='" + username + '\'' + " Not Found}";
        }
    }

    public static class ExtentionNotAllowed{
        public String ext;
        public List<String> allowedExts;

        public ExtentionNotAllowed(String ext, List<String> allowedExts) {
            this.ext = ext;
            this.allowedExts = allowedExts;
        }

        @Override
        public String toString() {
            return "{" + "ext='" + ext + '\'' + ", allowedExts=" + allowedExts + '}';
        }
    }

    public static class PasswordError{
        public String passwrod;

        public PasswordError(String passwrod) {
            this.passwrod = passwrod;
        }

        @Override
        public String toString() {
            return "{" + "password ='" + passwrod + '\'' + " must be at least 8 characters}";
        }
    }
}
