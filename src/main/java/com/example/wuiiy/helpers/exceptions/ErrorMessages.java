package com.example.wuiiy.helpers.exceptions;

import com.example.wuiiy.helpers.validator.Extention;

import java.util.HashMap;
import java.util.Map;

public class ErrorMessages {
    private static final Map<String, String> DEFINED_ERROR_MSGS = new HashMap<>();

    static {
        // Common error messages
        DEFINED_ERROR_MSGS.put("0", "Unauthorize");
        DEFINED_ERROR_MSGS.put("1", "Undefined error code: {err_code}");
        DEFINED_ERROR_MSGS.put("2", "Invalid {something}");
        DEFINED_ERROR_MSGS.put("3", "param {param} is required");
        DEFINED_ERROR_MSGS.put("4", "param {param} must be {something}");
        DEFINED_ERROR_MSGS.put("5", "Invalid {sso_type} token");
        DEFINED_ERROR_MSGS.put("6", "{data} already used");
        DEFINED_ERROR_MSGS.put("7", "{data} not registered");
        DEFINED_ERROR_MSGS.put("8", "email has verified");
        DEFINED_ERROR_MSGS.put("9", "ext {ext} is not in allowed's ext [{alloweds_ext}]");
        DEFINED_ERROR_MSGS.put("10", "no {something} available");
        DEFINED_ERROR_MSGS.put("11", "doctors {doctors} was added on this schedule");
        DEFINED_ERROR_MSGS.put("12", "{data} was expired");
        DEFINED_ERROR_MSGS.put("13", "payment gateway error {err_resp}");
        DEFINED_ERROR_MSGS.put("14", "wrong email or password");
        DEFINED_ERROR_MSGS.put("15", "username {username} not found");
    }

    public static void errorMsg(){
        Map<String, String> params = new HashMap<>();
        params.put("err_code", "404");
        params.put("something", "value");
        params.put("param", "username");
        params.put("data", "item");
        params.put("sso_type", "OAuth");
        params.put("ext", ".exe");
        params.put("alloweds_ext", ".jpg, .png");
        params.put("doctors", "Dr. Smith");
        params.put("err_resp", "Timeout");
    }

    public static String getErrorMessage(String errorCode, Map<String, String> params) {
        String message = DEFINED_ERROR_MSGS.get(errorCode);
        if (message != null && params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                message = message.replace("{" + entry.getKey() + "}", entry.getValue());
            }
        }
        return message;
    }
}