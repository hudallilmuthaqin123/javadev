package com.example.wuiiy.helpers.exceptions;

public class Response {
    private boolean isSuccess;
    private String message;

    public Response(boolean isSuccess, String message) {
        this.isSuccess = isSuccess;
        this.message = message;
    }

    public Response(String message) {
        this.message = message;
    }

    // Getters and setters
    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
