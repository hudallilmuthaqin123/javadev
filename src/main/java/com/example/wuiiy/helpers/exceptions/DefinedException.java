package com.example.wuiiy.helpers.exceptions;

import com.example.wuiiy.helpers.validator.Extention;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public class DefinedException extends RuntimeException{
    private String code;
    private Object detail;
    private String message;

    private  ResponseEntity<Object> httpResponseType;

    public DefinedException(String code, Object detail) {
        super("Error Code: " + code + ", Detail: " + detail.toString());
        this.code = code;
        this.detail = detail;
    }

    public DefinedException(String code, Object detail, String message) {
        super("Error Code: " + code + ", Message: " + message);
        this.code = code;
        this.detail = detail;
        this.message = message;
    }

    public DefinedException(String code, String message) {
        super("Error Code: " + code + ", Message: " + message);
        this.code = code;
        this.message = message;
    }

    public DefinedException(String code, Object detail, ResponseEntity<Object> objectResponseEntity, String message) {
        super("Error Code: " + code + ", Detail: " + detail);
        this.code = code;
        this.detail = detail;
        this.message = message;
        this.httpResponseType = objectResponseEntity;
    }

    public String getCode() {
        return code;
    }

    public Object getDetail() {
        return detail;
    }
}
