package com.example.wuiiy.helpers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public class HttpResponse {

    public static ResponseEntity<Response> okRequest() {
        Response okResponse = new Response(true,"success");
        return new ResponseEntity<>(okResponse, HttpStatus.OK);
    }

    public static ResponseEntity<Response> badRequest(String message) {
        Response response = new Response(false,message);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<Response> notFound(String message) {
        Response response = new Response(false,message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    public static ResponseEntity<Response> forbidden(String message) {
        Response response = new Response(false,message);
        return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
    }

    public static ResponseEntity<Response> notAllowed(String message) {
        Response response = new Response(false,message);
        return new ResponseEntity<>(response, HttpStatus.METHOD_NOT_ALLOWED);
    }
}
