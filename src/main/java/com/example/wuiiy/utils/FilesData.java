package com.example.wuiiy.utils;

import com.example.wuiiy.helpers.validator.Extention;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import java.io.IOException;

public class FilesData {

    public static String domain = "http://localhost:8080" + java.io.File.separator;

    static String splitData = "\r\n|\r|\n";

    public static String generateFilePathandSave(String dirType, String accountId, MultipartFile files) {
        String uploadDir = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        String fullpath = dirType+ java.io.File.separator + accountId.toString() + java.io.File.separator;
        java.io.File uploadPath = new java.io.File(fullpath);
        if (!uploadPath.exists()) {
            uploadPath.mkdirs();
        }
        saveFile(fullpath, files);
        return fullpath + files.getOriginalFilename();
    }

    public static void saveFile(String filepath, MultipartFile file) {
        java.io.File uploadedFile = new java.io.File(filepath, file.getOriginalFilename());
        try (OutputStream outputStream = new FileOutputStream(uploadedFile);
            InputStream inputStream = file.getInputStream()) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] readContentFromPath(String path) {

        Path filePath = Paths.get(path).normalize();
        File file = filePath.toFile();

        if (!file.exists()) {
            return null; // or throw FileNotFoundException
        }

        try (FileInputStream fis = new FileInputStream(file);
             ByteArrayOutputStream resource = new ByteArrayOutputStream()) {

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fis.read(buffer)) != -1) {
                resource.write(buffer, 0, bytesRead);
            }

            return resource.toByteArray();

        } catch (IOException ex) {
            ex.printStackTrace(); // Handle the exception appropriately
            return null;
        }
    }

    public static String readFile(MultipartFile file) throws IOException {
        Extention.validateDocs(Objects.requireNonNull(file.getOriginalFilename()));
        return new String(file.getBytes(), StandardCharsets.UTF_8);
    }

    public static String getHeader(String content) {
        String[] lines = content.split(splitData);
        for (String line : lines) {
            if (line.startsWith("H")) {
                return line;
            }
        }
        return "Header Not Found.";
    }

    public static List<String> getBody(String content) {
        List<String> bodyLines = new ArrayList<>();
        String[] lines = content.split(splitData);
        for (String line : lines) {
            if (line.startsWith("D")) {
                bodyLines.add(line);
            }
        }
        if (bodyLines.isEmpty()) {
            bodyLines.add("Body Not Found.");
        }
        return bodyLines;
    }

    public static String getFooter(String content) {
        String[] lines = content.split(splitData);
        for (String line : lines) {
            if (line.startsWith("T")) {
                return line;
            }
        }
        return "Footer Not Found.";
    }
}

