package com.example.wuiiy.controller.users;


import com.example.wuiiy.config.security.JwtTokenUtil;
import com.example.wuiiy.helpers.exceptions.HttpResponse;
import com.example.wuiiy.helpers.exceptions.Response;
import com.example.wuiiy.model.JwtResponse;
import com.example.wuiiy.model.dto.users.UsersDTO;
//import com.example.wuiiy.service.users.JwtUserDetailsService;
import com.example.wuiiy.service.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
public class Register {

    private static final Logger logger = LoggerFactory.getLogger(Register.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UsersService usersService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> createUsers(@RequestBody UsersDTO data){
            UsersDTO usersDTO = usersService.createUser(data);
            return HttpResponse.okRequest();
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody UsersDTO authenticationRequest) throws Exception {
//        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        usersService.validateUser(authenticationRequest.getUsername());
        final UserDetails userDetails = usersService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @GetMapping("/get-username")
    public ResponseEntity<?> getUsernameFromToken(@RequestHeader("Authorization") String authorizationHeader) {
        String token = jwtTokenUtil.extractTokenFromHeader(authorizationHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        return ResponseEntity.ok(new UsernameResponse(username));
    }

    public static class UsernameResponse {
        private String username;

        public UsernameResponse(String username) {
            this.username = username;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
