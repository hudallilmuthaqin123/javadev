package com.example.wuiiy.controller.users;

import com.example.wuiiy.model.dto.users.UsersDTO;
import com.example.wuiiy.service.users.UsersService;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@RestController
public class FileDownload {

    private static final Logger logger = LoggerFactory.getLogger(FileDownload.class);

    @Autowired
    private UsersService usersService;

    @GetMapping("/excelexport")
    public void exportExcel(HttpServletResponse response) throws IOException {

        // Set file name and content type
        String filename = "Employee-data.xlsx";
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"");

        // Create Excel workbook and sheet
        Workbook workbook = WorkbookFactory.create(true);
        Sheet sheet = workbook.createSheet("Form Reports");

        // Create header style
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerStyle.setFont(headerFont);
        headerStyle.setAlignment(HorizontalAlignment.LEFT);

        // Create content style
        CellStyle contentStyle = workbook.createCellStyle();
        contentStyle.setAlignment(HorizontalAlignment.LEFT);

        // Create header row
        Row headerRow = sheet.createRow(0);
        String[] headerTitles = {"ID", "FORMNAME", "FORMDESCRIPTION"};
        for (int i = 0; i < headerTitles.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headerTitles[i]);
            cell.setCellStyle(headerStyle);
        }

        // Write form reports data to Excel file
        int rowNum = 1;
        for (UsersDTO usersData : usersService.listUsers()) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(1).setCellValue(usersData.getUsername());
            row.createCell(2).setCellValue(usersData.getPassword());

            // Apply content style to each cell in the current row
            for (Cell cell : row) {
                cell.setCellStyle(contentStyle);
            }
        }

        // Auto-size columns
        for (int i = 0; i < headerTitles.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the workbook to the response
        workbook.write(response.getOutputStream());
        workbook.close();
    }
}
