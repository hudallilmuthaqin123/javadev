package com.example.wuiiy.controller;

import com.example.wuiiy.helpers.exceptions.HttpResponse;
import com.example.wuiiy.helpers.exceptions.Response;
import com.example.wuiiy.model.dto.files.Settlement;
import com.example.wuiiy.service.users.UsersService;
import com.example.wuiiy.utils.FilesData;
import com.example.wuiiy.helpers.validator.Extention;
import com.example.wuiiy.utils.Properties;
import com.example.wuiiy.utils.TypeDirectory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@RestController
public class Test {

    private static final Logger logger = LoggerFactory.getLogger(Test.class);

    @Autowired
    private UsersService usersService;

    @Autowired
    private Properties properties;

    @RequestMapping(value = "/hello/{name}", method = RequestMethod.GET)
    public String hello(@PathVariable("name") String name){
        return "Hello World";
    }

    @RequestMapping({ "/hello" })
    public String firstPage() {
        return "Hello World";
    }
    @GetMapping("/bad-request")
    public ResponseEntity<Response> badRequestEndpoint() {
        return HttpResponse.badRequest("Bad Request");
    }
    @GetMapping("/ok-request")
    public ResponseEntity<Response> okRequestEndpoint() {
        return HttpResponse.okRequest();
    }
    @GetMapping("/not-found")
    public ResponseEntity<Response> notFoundEndpoint() {
        return HttpResponse.notFound("Not Found");
    }

    @GetMapping("/forbidden")
    public ResponseEntity<Response> forbiddenEndpoint() {
        return HttpResponse.forbidden("Forbidden");
    }

    @GetMapping("/not-allowed")
    public ResponseEntity<Response> notAllowedEndpoint() {
        return HttpResponse.notAllowed("Method Not Allowed");
    }

    @RequestMapping({ "/test-json" })
    public ResponseJson testJson(@RequestParam("name") Object json) {
        String jsonString = json.toString();

        // Mengonversi string JSON menjadi objek JSON menggunakan Gson
        Gson gson = new Gson();
        ResponseJson person = gson.fromJson(jsonString, ResponseJson.class);

        // Mengakses nilai dari objek JSON
        String username = person.getUsername();
        String password = person.getPassword();

        return new ResponseJson(username, password);
    }

    public static class ResponseJson {
        private String username;
        private String password;

        @JsonCreator
        public ResponseJson(@JsonProperty("username") String username, @JsonProperty("password") String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    @PostMapping("/upload")
    public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return HttpResponse.badRequest("Please select a file to upload");
        }
        Extention.validateImgs(Objects.requireNonNull(file.getOriginalFilename()));
        String getUsername = usersService.getUsername("ini_username_12").getUsername();
        String filepath = FilesData.generateFilePathandSave(properties.DirKTP,getUsername,file);
        byte[] fileContent = FilesData.readContentFromPath(filepath);
//        return HttpResponse.okRequest();
        String contentType = "image/jpeg";
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                .body(fileContent);
    }

    @GetMapping("/readfilecontent")
    public ResponseEntity<?> readFile2() {

        Path filePath = Paths.get(properties.baseDirktp()).normalize();
        File file = filePath.toFile();

        if (!file.exists()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("File not found");
        }

        try {
            FileInputStream fis = new FileInputStream(file);
            InputStreamResource resource = new InputStreamResource(fis);

            // Determine content type
            String contentType = "image/jpeg"; // You can determine the content type based on the file extension or other logic

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                    .body(resource);

        } catch (IOException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error reading file: " + ex.getMessage());
        }
    }

    @GetMapping("/read")
    public ResponseEntity<Settlement> readFile(
            @RequestParam("file") MultipartFile file
    ) {
        Settlement settlement = new Settlement();
        try {
            String content = FilesData.readFile(file);
            String header = FilesData.getHeader(content);
            List<String> body = FilesData.getBody(content);
            String footer = FilesData.getFooter(content);

            settlement.setHeader(header);
            settlement.setData(body);
            settlement.setFooter(footer);
            return ResponseEntity.status(HttpStatus.OK).body(settlement);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(settlement);
        }
    }
}
