package com.example.wuiiy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WuiiyApplication {

	public static void main(String[] args) {
		SpringApplication.run(WuiiyApplication.class, args);
	}

}
