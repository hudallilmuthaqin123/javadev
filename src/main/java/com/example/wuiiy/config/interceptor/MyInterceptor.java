package com.example.wuiiy.config.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Component
public class MyInterceptor implements HandlerInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(MyInterceptor.class);
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("===============Pre Handle method is Calling===============");
        logData(request);
//        logRequestProcessingTime(request);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        logger.info("===============Post Handle method is Calling===============");
        if (modelAndView != null) {
            // Manipulate the data before rendering the view
            modelAndView.addObject("additionalData", "This is additional data for the view");
        }

        if (modelAndView != null) {
            // Modify the response before sending it to the client
            response.addHeader("Custom-Header", "This is a custom header");
            response.getWriter().write("This is a custom response body");
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
        logger.info("===============Request and Response is completed===============");
        // Logging auditing log
        String auditLogMessage = "Request to: " + request.getRequestURI() + " | Status: " + response.getStatus();
        logger.info(auditLogMessage);

        // Check for errors
        if (exception != null) {
            // Handle the exception and send an appropriate error response to the client
            int status = response.getStatus();
            if (status == HttpServletResponse.SC_NOT_FOUND) {
                String errorMessage = "Resource not found for URI: " + request.getRequestURI();
                response.sendError(HttpServletResponse.SC_NOT_FOUND, errorMessage);
                logger.error(errorMessage);
            } else {
                // Handle the exception from the service
                String serviceErrorMessage = "Error occurred in service: " + exception.getMessage();
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, serviceErrorMessage);
                logger.error(serviceErrorMessage);
            }
        }
    }

    private void logData(HttpServletRequest request) throws Exception {
        Map<String, String> pathVariables = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

        // Get request parameters
        Map<String, String[]> requestParams = request.getParameterMap();
        // Convert requestParams to a readable format
        Map<String, String> readableParams = new HashMap<>();
        for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
            String paramName = entry.getKey();
            String[] paramValues = entry.getValue();
            String paramValue = (paramValues.length == 1) ? paramValues[0] : Arrays.toString(paramValues);
            readableParams.put(paramName, paramValue);
        }

        // Get request headers
        Enumeration<String> headerNames = request.getHeaderNames();
        Map<String, String> requestHeaders = new HashMap<>();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = request.getHeader(headerName);
            requestHeaders.put(headerName, headerValue);
        }

        logger.info("Request URI: {}", request.getRequestURI());
        logger.info("Request Method: {}", request.getMethod());
        if (!readableParams.isEmpty())
            logger.info("Request Parameters: {}", readableParams);
        if (!pathVariables.isEmpty()){
            logger.info("Request PathVariables: {}", pathVariables);
        }
        logger.info("Request Headers: {}", requestHeaders);
//        logger.info("Request Body: {}", request.getReader());
    }

    private void logRequestProcessingTime(HttpServletRequest request) {
        long startTime = (Long) request.getAttribute("startTime");
        long endTime = System.currentTimeMillis();
        long processingTime = endTime - startTime;
        logger.info("Request processing time: {}ms", processingTime);
    }
}
