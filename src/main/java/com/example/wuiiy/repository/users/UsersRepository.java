package com.example.wuiiy.repository.users;

import com.example.wuiiy.model.entity.users.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<UsersEntity, Long> , QuerydslPredicateExecutor<UsersEntity> {
    Optional<UsersEntity> findByUsername(String username);
//    Optional<UsersEntity> findByUsername(String username, String password);
}
