package com.example.wuiiy.service.users;

import com.example.wuiiy.helpers.auth.Hashed;
import com.example.wuiiy.helpers.exceptions.DefinedException;
import com.example.wuiiy.helpers.exceptions.ErrorMessages;
import com.example.wuiiy.model.dto.users.UsersDTO;
import com.example.wuiiy.model.entity.users.QUsersEntity;
import com.example.wuiiy.model.entity.users.UsersEntity;
import com.example.wuiiy.repository.users.UsersRepository;
import com.example.wuiiy.helpers.validator.SecurePassword;
import com.example.wuiiy.helpers.exceptions.ExceptionDetail;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UsersService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(UsersService.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UsersRepository usersRepository;

    public UsersDTO createUser(UsersDTO usersDTO) {
        try {
            String username = usersDTO.username;
            String password = usersDTO.password;

            SecurePassword.validatePassword(password);
            String hashedPassword = Hashed.Password(password);

            UsersEntity usersEntity = new UsersEntity();
            usersEntity.setUsername(username);
            usersEntity.setPassword(hashedPassword);
            usersRepository.save(usersEntity);
            return usersDTO;
        } catch (Exception e) {
            logger.error("Error createUser", e.getMessage());
            throw e;
        }
    }

    public UsersDTO getUsername(String username) {
        try {
            UsersDTO usersDTO = new UsersDTO();
            BooleanBuilder builder = new BooleanBuilder();
            QUsersEntity qUsers = QUsersEntity.usersEntity;
            builder.and(qUsers.username.contains(username));
            Optional<UsersEntity> response = usersRepository.findOne(builder);
            if (response.isPresent()) {
                usersDTO.setUsername(response.get().getUsername());
                usersDTO.setPassword(response.get().getPassword());
            }
            return usersDTO;
        } catch (Exception e) {
            logger.error("Error createUser", e.getMessage());
            throw e;
        }
    }

    public List<UsersDTO> listUsers() {
        try {
            List<UsersDTO> listUsers =new ArrayList<>();
//            BooleanBuilder builder = new BooleanBuilder();
            JPAQueryFactory query = new JPAQueryFactory(em);
            QUsersEntity qUsers = QUsersEntity.usersEntity;
            List<UsersEntity> lsResp = query.selectFrom(qUsers).fetch().stream().collect(Collectors.toList());
            if(!lsResp.isEmpty()) {
//                int nSdata = 0;
                for(UsersEntity response : lsResp) {
                    UsersDTO usersDTO = new UsersDTO();
                    usersDTO.setUsername(response.getUsername());
                    usersDTO.setPassword(response.getPassword());
                    listUsers.add(usersDTO);
                }
            }
            return listUsers;
        } catch (Exception e) {
            logger.error("Error createUser", e.getMessage());
            throw e;
        }
    }

    public void validateUser(String username) {
        try {
            BooleanBuilder builder = new BooleanBuilder();
            QUsersEntity qUsers = QUsersEntity.usersEntity;
            builder.and(qUsers.username.contains(username));
            Optional<UsersEntity> response = usersRepository.findOne(builder);
            if (!response.isPresent()) {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                String errorMsg = ErrorMessages.getErrorMessage("15", params);
                throw new DefinedException("15",new ExceptionDetail.UsernameNotFound(username),errorMsg);
            }
        } catch (Exception e) {
            logger.error("Error validateUser", e.getMessage());
            throw e;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UsersEntity user = usersRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
    }
}
//    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
//    public UserDetails getUserTest(String username) throws UsernameNotFoundException {
//        try {
//            UsersDTO usersDTO = new UsersDTO();
//            BooleanBuilder builder = new BooleanBuilder();
//            QUsersEntity qUsersEntity = QUsersEntity.usersEntity;
//            Optional<UsersEntity> usersEntity= usersRepository.findByUsername(username);
//            if(usersEntity.isPresent()) {
//                usersDTO.setUsername(usersEntity.get().getUsername());
//                usersDTO.setPassword(usersEntity.get().getPassword());
//
//            } else {
//                throw new UsernameNotFoundException("User not found with username: " + username);
//            }
//            return new org.springframework.security.core.userdetails.User(usersEntity.get().getUsername(),
//                    usersEntity.get().getPassword(),
//                    new ArrayList<>());
//        } catch (Exception e) {
//            logger.error("Error getUser", e.getMessage());
//            throw e;
//        }
//    }


//    Optional<UsersEntity> userOptional = usersRepository.findByUsername(username);
//            if (userOptional.isPresent()) {
//                    UsersEntity userEntity = userOptional.get();
//                    UsersDTO usersDTO = convertToDTO(userEntity);
//                    return usersDTO;
//                    } else {
//                    // Handle kasus jika pengguna tidak ditemukan
//                    // Contoh: throw new UserNotFoundException("User with username " + username + " not found");
//                    Map<String, String> params = new HashMap<>();
//        params.put("param", username);
//        String errorMsg = ErrorMessages.getErrorMessage("3", params);
//        throw new DefinedException("3",errorMsg);
//        }
